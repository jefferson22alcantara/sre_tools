# sre_tools


## bitbucket api 
```
usage: bitbucket_api.py [-h] [--listprojects] [--listrepos] [--create_project_dirs] [--project PROJECT] [--user USER] [--password PASSWORD]
                        [--clone_all]

    Git bitbucket api tool
    obs: pip install -r requirements.txt
    ./bitbucket_api.py --listprojects --user $USER --pass BITBUCKET_API_KEY | ./bitbucket_api.py --listrepos --project ACC --user $USER --pass BITBUCKET_API_KEY | ./bitbucket_api.py --clone_all --project ACC  --user $USER --pass BITBUCKET_API_KEY | --create_project_dirs --user $USER --pass BITBUCKET_API_KEY



optional arguments:
  -h, --help            show this help message and exit
  --listprojects        list bitbucket projects
  --listrepos           list bitbucket repos from project
  --create_project_dirs
                        create all projects bitbucket dirs on current directory
  --project PROJECT      a bitbucket project name from --listproject
  --user USER           bitbucket user
  --password PASSWORD   bitbucket api key password
  --clone_all            Clone all repos from project
```

![USAGE](bitbucket_api_v3.gif)



Sre tools scripts 
